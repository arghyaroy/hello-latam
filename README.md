[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/hugoazevedo/hello-latam)

# Tanuki Logo
O logo Tanuki incluído neste projeto foi obtido a partir de [codepen](https://codepen.io/heyMP/pen/LNjeOM).  
Inspirado em um projeto do [Michael Friedrich](https://gitlab.com/dnsmichi/animated-tanuki).  
Obrigado ao @mattdark pelo design da página Web utilizada no projeto, incluindo [Single Page Transition](https://codepen.io/ktsn/pen/wrxymV)
Obrigado ao @ricardo.amarilla por ter criado a versão original em Espanhol para que pudéssemos traduzir para o Português!
## Preparação
Os passos para acompanhar esse exercício prático se encontram [nesta  apresentação](https://docs.google.com/presentation/d/1ezyl_qjbxhggKnGgb4vAHmk_gXi098LmSY4M0Sp-yzk/edit?usp=sharing)

Hoje é 17 de junho de 2020




